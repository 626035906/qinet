using AuthServer.Host;
using Serilog;
using Serilog.Events;

//TODO: Temporary: it's not good to read appsettings.json here just to configure logging
var configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json")
    .AddEnvironmentVariables()
    .Build();

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
    .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
    .Enrich.WithProperty("Application", "AuthServer")
    .Enrich.FromLogContext()
    .WriteTo.Console()
    //.WriteTo.File("Logs/logs.txt")
    // .WriteTo.Elasticsearch(
    //     new ElasticsearchSinkOptions(new Uri(configuration["ElasticSearch:Url"]))
    //     {
    //         AutoRegisterTemplate = true,
    //         AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv6,
    //         IndexFormat = "msdemo-log-{0:yyyy.MM}"
    //     })
    .CreateLogger();

try
{
    Log.Information("Starting AuthServer.Host.");
    
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.AddAppSettingsSecretsJson()
        .UseAutofac()
        .UseSerilog();
    
    await builder.AddApplicationAsync<AuthServerHostModule>();

    var app = builder.Build();

    app.InitializeApplication();
    //app.MapGet("/", () => Results.Redirect("swagger"));

    app.Run();
    
    return 0;
}
catch (Exception ex)
{
    Log.Fatal(ex, "AuthServer.Host terminated unexpectedly!");
    return 1;
}
finally
{
    Log.CloseAndFlush();
}
