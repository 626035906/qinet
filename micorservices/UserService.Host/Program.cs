using UserService.Host;

var builder = WebApplication.CreateBuilder(args);

builder.Host.AddAppSettingsSecretsJson()
    .UseAutofac();

builder.Services.AddApplication<UserServiceHostModule>();

var app = builder.Build();

app.Run();